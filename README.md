## Final Project

## Anggota Kelompok

-   Muhammad Fahri Ramadhan (@fahri4321)
-   Sri Oktari (@srioktari)
-   Azwar Rusli (@azwarrr29)
-   Andre (@andresmks00)
-   Revelino Fitra Fahmi (@Ravelinoo)

## Tema Project

Forum Disku

## Penjelasan

Sistem forum tanya jawab ini dibangun menggunakan laravel 8 yang terdiri dari user biasa dan admin adapun menu/fitur yang terdapat
disistem ini sebagai berikut:

-   Crud Pertannyaan
-   Input Komentar Pertanyaan
-   Crud Jawaban
-   Input Komentar Jawaban
-   Crud Kategori
-   Crud User
-   Edit Profile

Adapun alur sistem forum tanya jawab ini adalah, pertama user harus registrasi untuk membuat akun setelah akun dibuat kemudian login jika berhasil login user
akan ditampilkan halaman list pertanyaan user dapat membuat, mengedit dan menghapus pertanyaan kemudian user dapat mengcrud kategori, user juga dapat memberikan komentar pada pertanyaan dan jawban, user juga dapat mengubah jawaban yang telah diinput, user juga dapat mengubah profile.

Halaman admin terdapat menu forum tanya jawab seperti user biasa dan admin pada halaman admin berfungsi untuk mengelola data tanya jawab crud pertanyaan, jawaban, user dan kategori.

## ERD

<p align="center"><img src="public/images/erd dsiku.png"></p>

## Link Video

-   Link Google Drive Video Aplikasi : [https://drive.google.com/drive/folders/1CBxqpKgigWCB3NfECnm7T1Tm3jQjPEdx?usp=share_link](https://drive.google.com/drive/folders/1CBxqpKgigWCB3NfECnm7T1Tm3jQjPEdx?usp=share_link).
-   Link Deploy : [https://disku.ezscode.com/](https://disku.ezscode.com/).
